# cookies.js

## Install

1. Скачать библиотеку локально
2. Подключить скрипт в header

## Usage

* Возвращает значение cookie с переданным ключом sKey:

`docCookies.getItem(skey);`

* Устанавливает cookie с названием sKey и значением sValue. Остальные параметры необязательны и используются для более точного задания параметров cookie:

`docCookies.setItem(sKey, sValue, vEnd, sPath, sDomain, bSecure);`

* Удаляет cookie по переданному ключу. sPath и sDomain необязательные параметры:

`docCookies.removeItem(sKey, sPath, sDomain);`

* Проверяет, действительно ли существует cookie с переданным названием:

`docCookies.hasItem(sKey);`

* Возвращает все ключи, установленных cookie:

`docCookies.keys();`

## Explanations

Создавал этот репозиторий т.к. на тот момент библиотека `cookies.js` была удалена из официальной документации MDN, но я нашёл исходники в архивах и сохранил в этом репозитории.

Сейчас [оф.дока MDN](https://developer.mozilla.org/en-US/docs/Web/API/document/cookie) вновь ссылается на библиотеку `cookies.js` [Simple cookie framework](https://developer.mozilla.org/en-US/docs/Web/API/Document/cookie/Simple_document.cookie_framework).

Ссылка на репозиторий с библиотекой [cookies.js](https://github.com/madmurphy/cookies.js).
